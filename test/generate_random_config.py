import numpy as np

peak_offset = 10

f = open('random.txt', 'wb')

for i in xrange(100):
    shift_x = np.random.random()*peak_offset
    shift_y = np.random.random()*peak_offset
    shift_z = np.random.random()*peak_offset
    phi = np.random.randint(360)
    psi = np.random.randint(360)
    the = np.random.randint(180)

    f.write("%f %f %f %d %d %d\n" % (shift_x, shift_y, shift_z, phi, psi, the))


f.close()

