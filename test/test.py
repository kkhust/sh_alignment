from sh_alignment.tompy.io import *
from sh_alignment.tompy.filter import SingleTiltWedge
from sh_alignment.tompy.transform import rotate3d, translate3d
from sh_alignment.tompy.tools import rotation_distance, euclidian_distance, add_noise
from sh_alignment.frm import frm_align
import numpy as np

v = read('80SRibosome.em')

# simulation settings
wedge = SingleTiltWedge(-60,60)
b = [4, 64]
freq = 20
peak_offset = int(10*3**0.5)+1

data =  np.genfromtxt('random.txt')

for i in xrange(10):
    shift_x = data[i, 0]
    shift_y = data[i, 1]
    shift_z = data[i, 2]
    phi = data[i, 3]
    psi = data[i, 4]
    the = data[i, 5]

    # print shift_x, shift_y, shift_z, phi, psi, the

    v2 = rotate3d(v, phi, psi, the)
    v2 = translate3d(v2, shift_x, shift_y, shift_z)
    # v2 = add_noise(v2, 0.01)
    v3 = wedge.apply(v2)

    # FRM
    pos, ang, score = frm_align(v3, wedge, v, None, b, freq, peak_offset)
    print 'translation error (pixel):', euclidian_distance([v.shape[0]/2+shift_x,v.shape[1]/2+shift_y,v.shape[2]/2+shift_z], pos)
    print 'rotation error (degree):', rotation_distance([phi, psi, the], ang)
    print 'correlation score:', score
    print


